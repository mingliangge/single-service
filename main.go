package main

import (
	"gin-service/global"
	"gin-service/initialize"
	"gin-service/router"
)

//go:generate git remote remove origin
//go:generate go env -w GO111MODULE=on
//go:generate go env -w GOPROXY=https://goproxy.cn,direct
//go:generate go install github.com/cosmtrek/air@v1.29.0
//go:generate go install github.com/swaggo/swag/cmd/swag@latest
//go:generate go mod tidy
//go:generate go mod download
//go:generate swag init

// @title 一个简单的 API 文档
// @version 1.0
// @description 一看就懂，一练就会
// termsOfService http://baidu.com // 服务条款

// @contact.name 技术支持
// @contact.url https://github.com/swaggo/swag/blob/master/README_zh-CN.md
// @contact.email xiaoliang0730@163.com

// token 头部定义
// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name x-token

// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html

// @host localhost:8080
// BasePath /api

func main() {
	defer func() {
		if err := recover(); err != nil {
			global.LOG.Error(err)
		}
	}()

	initialize.Preload()

	global.Init()

	router.Run()
}
