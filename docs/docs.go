// Package docs GENERATED BY THE COMMAND ABOVE; DO NOT EDIT
// This file was generated by swaggo/swag
package docs

import "github.com/swaggo/swag"

const docTemplate = `{
    "schemes": {{ marshal .Schemes }},
    "swagger": "2.0",
    "info": {
        "description": "{{escape .Description}}",
        "title": "{{.Title}}",
        "contact": {
            "name": "技术支持",
            "url": "https://github.com/swaggo/swag/blob/master/README_zh-CN.md",
            "email": "xiaoliang0730@163.com"
        },
        "license": {
            "name": "Apache 2.0",
            "url": "http://www.apache.org/licenses/LICENSE-2.0.html"
        },
        "version": "{{.Version}}"
    },
    "host": "{{.Host}}",
    "basePath": "{{.BasePath}}",
    "paths": {
        "/user/gen_token": {
            "get": {
                "security": [
                    {
                        "ApiKeyAuth": []
                    }
                ],
                "tags": [
                    "用户相关"
                ],
                "summary": "生成token",
                "responses": {}
            }
        },
        "/user/login": {
            "post": {
                "tags": [
                    "用户相关"
                ],
                "summary": "用户登录",
                "parameters": [
                    {
                        "description": "大量数据",
                        "name": "data",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/request.LoginForm"
                        }
                    }
                ],
                "responses": {}
            }
        },
        "/user/register": {
            "post": {
                "tags": [
                    "用户相关"
                ],
                "summary": "一段简明扼要的话",
                "parameters": [
                    {
                        "maxLength": 10,
                        "minLength": 1,
                        "type": "string",
                        "description": "密码",
                        "name": "password",
                        "in": "formData",
                        "required": true
                    },
                    {
                        "maxLength": 10,
                        "minLength": 1,
                        "type": "string",
                        "description": "重复密码",
                        "name": "repeat_password",
                        "in": "formData",
                        "required": true
                    },
                    {
                        "type": "string",
                        "description": "用户名",
                        "name": "username",
                        "in": "formData",
                        "required": true
                    }
                ],
                "responses": {}
            }
        },
        "/user/token_use": {
            "get": {
                "security": [
                    {
                        "ApiKeyAuth": []
                    }
                ],
                "tags": [
                    "用户相关"
                ],
                "summary": "使用token",
                "responses": {}
            }
        }
    },
    "definitions": {
        "request.LoginForm": {
            "type": "object",
            "required": [
                "captcha",
                "captcha_id",
                "password",
                "username"
            ],
            "properties": {
                "captcha": {
                    "description": "用户名 给 swagger 看",
                    "type": "string"
                },
                "captcha_id": {
                    "description": "用户名 给 swagger 看",
                    "type": "string"
                },
                "password": {
                    "description": "用户名 给 swagger 看",
                    "type": "string"
                },
                "username": {
                    "description": "用户名 给 swagger 看",
                    "type": "string",
                    "maxLength": 10,
                    "minLength": 3
                }
            }
        }
    },
    "securityDefinitions": {
        "ApiKeyAuth": {
            "type": "apiKey",
            "name": "x-token",
            "in": "header"
        }
    }
}`

// SwaggerInfo holds exported Swagger Info so clients can modify it
var SwaggerInfo = &swag.Spec{
	Version:          "1.0",
	Host:             "localhost:8080",
	BasePath:         "",
	Schemes:          []string{},
	Title:            "一个简单的 API 文档",
	Description:      "一看就懂，一练就会",
	InfoInstanceName: "swagger",
	SwaggerTemplate:  docTemplate,
}

func init() {
	swag.Register(SwaggerInfo.InstanceName(), SwaggerInfo)
}
