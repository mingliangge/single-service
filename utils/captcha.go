package utils

import (
	"gin-service/model/response"

	"github.com/gin-gonic/gin"
	"github.com/mojocn/base64Captcha"
)

var CaptchaStore = base64Captcha.DefaultMemStore

func GetCapcha(ctx *gin.Context) {
	driver := base64Captcha.NewDriverDigit(80, 240, 4, 0.7, 80)
	cp := base64Captcha.NewCaptcha(driver, CaptchaStore)
	id, b64s, err := cp.Generate()
	if err != nil {
		response.FailWithMessage("生成验证码错误", ctx)
		return
	}
	response.OkWithData(gin.H{
		"id":  id,
		"b64": b64s,
	}, ctx)
}
