package config

type ConfigData struct {
	Namespace string `mapstructure:"namespace"`

	Gin struct {
		Mode string `mapstructure:"mode"`
		Port string `mapstructure:"port"`
	} `mapstructure:"gin"`

	Zap struct {
		Level      int    `mapstructure:"level"`
		Filename   string `mapstructure:"filename"`
		MaxSize    int    `mapstructure:"max-size"`
		MaxBackups int    `mapstructure:"max-backups"`
		MaxAge     int    `mapstructure:"max-age"`
		Compress   bool   `mapstructure:"compress"`
	} `mapstructure:"zap"`

	Mysql struct {
		Path     string `mapstructure:"path"`
		Port     string `mapstructure:"port"`
		Config   string `mapstructure:"config"`
		DbName   string `mapstructure:"db-name"`
		Username string `mapstructure:"username"`
		Password string `mapstructure:"password"`
	} `mapstructure:"mysql"`

	Cors struct {
		Mode      string      `mapstructure:"mode"`
		Whitelist []Whitelist `mapstructure:"whitelist"`
	} `mapstructure:"cors"`

	JWT struct {
		SigningKey  string `mapstructure:"signing-key"`  // jwt签名
		ExpiresTime int64  `mapstructure:"expires-time"` // 过期时间
		BufferTime  int64  `mapstructure:"buffer-time"`  // 缓冲时间
		Issuer      string `mapstructure:"issuer"`       // 签发者
	} `mapstructure:"jwt"`

	Redis struct {
		DB       int    `mapstructure:"db"`
		ADDR     string `mapstructure:"addr"`
		PASSWORD string `mapstructure:"password"`
	} `mapstructure:"redis"`
}

type Whitelist struct {
	AllowOrigin      string `mapstructure:"allow-origin"`
	AllowMethods     string `mapstructure:"allow-methods"`
	AllowHeaders     string `mapstructure:"allow-headers"`
	ExposeHeaders    string `mapstructure:"expose-headers"`
	AllowCredentials bool   `mapstructure:"allow-credentials"`
}
