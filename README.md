# gin-service

#### 介绍
一个开箱即用的 gin 单体服务架构

#### 框架架构
```
├──────
    ├── api                          (数据校验)  
    ├── config                       (config 结构体)      
    ├── docs                         (api 文档)
    ├── global                       (全局工具)
    ├── initialize                   (框架初始化)
    ├── middleware                   (路由中间件)
    ├── model                        (结构体层)
    │   └── request                  (请求结构体)
    │   └── response                 (返回结构体)  
    │   └── xxx                      (数据表结构)  
    ├── router                       (全局路由)
    ├── service                      (业务逻辑层)
    ├── test                         (测试代码)
    ├── utils                        (工具集合)
    ├── .air.toml                    (热加载开发配置)
    ├── .gitignore          
    ├── Dockerfile                   (构建镜像)
    ├── go.mod              
    ├── go.sum              
    ├── LICENSE              
    ├── main.go                      (入口文件)
    ├── nacos.yaml                  (配置中心文件)
    ├── README.md                                

```

#### 安装教程
1. git clone https://gitee.com/mingliangge/gin-service.git
2. 搭建 nacos 配置中心，连接nacos写在nacos.yaml，配置中心文件示例在./config/example.yaml 
3. go generate

#### 使用说明
1. swag init (生成swagger文档)
2. air（热加载开发）
3. docker build -t mingliangge/gin-service:1.0 -f ./Dockerfile . (构建镜像)

#### 参与贡献
1.  Fork 本仓库
2.  新建 future 分支
3.  提交代码
4.  新建 Pull Request

#### 特技
1. 使用 nacos 配置中心，保证配置安全
2. 使用了 Zap 记录本地日志，并分片保存到本地
3. 使用了 Gorm 驱动数据库
4. 使用了 Gin 作为 Http 框架
5. 使用了 Redis 作为数据缓存层
6. 使用了 Air 做热加载开发
7. 使用了 base64Captcha 做图形验证码校验
8. 使用了 swag 自动构建api文档
