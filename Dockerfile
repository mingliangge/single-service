# 启动编译环境
FROM golang:1.17-alpine AS builder

# 配置编译环境
RUN go env -w GO111MODULE=on
RUN go env -w GOPROXY=https://goproxy.cn,direct

# 拷贝源代码到镜像中
COPY . /go/src/gin-service

# 编译
WORKDIR /go/src/gin-service
RUN go install .

FROM alpine:3.15
COPY --from=builder /go/bin/gin-service /usr/local/bin/gin-service
COPY --from=builder /go/src/gin-service/config.yaml /usr/local/config.yaml
ENV ADDR=:8080

# 申明暴露的端口
EXPOSE 8080

# 设置服务入口
WORKDIR /usr/local
ENTRYPOINT [ "./bin/gin-service" ]

# 打包
# docker build -t mingliangge/gin-service:1.0 -f ./Dockerfile .

# 运行 (--net=host)
# docker run -d --name gin-service -p 8080:8080 mingliangge/gin-service:1.0
# docker run -d --name gin-service --net host mingliangge/gin-service:1.0

# 日志
# docker logs 470c9ea1ff48

# 打 tag
# docker tag mingliangge/gin-service:1.0 xiaoliang0730/gin-service:1.0

# 上传
# docker push xiaoliang0730/gin-service:1.0