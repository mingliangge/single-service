package initialize

import (
	"gin-service/config"
	"gin-service/global"

	"github.com/spf13/viper"
)

// readConfig 读取配置
func readConfig() {

	v := viper.New()

	// 设置文件路径
	v.SetConfigFile(tempFileName)

	// 读取全部文件
	if err := v.ReadInConfig(); err != nil {
		panic(err)
	}

	// 解析配置文件
	c := config.ConfigData{}
	if err := v.Unmarshal(&c); err != nil {
		panic(err)
	}

	global.CONFIG = &c
}
