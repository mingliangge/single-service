package request

type RegisterForm struct {
	Username string	`json:"username" form:"username" binding:"required,min=3,max=10"`
	Password string	`json:"password" form:"password" binding:"required"`
	RepeatPassword string	`json:"repeat_password" form:"repeat_password" binding:"required,eqfield=Password"`
}

type LoginForm struct {
	Username string	`gorm:"comment:用户名" form:"username" json:"username" binding:"required,min=3,max=10"` // 用户名 给 swagger 看
	Password string	`gorm:"comment:密码" form:"password" json:"password" binding:"required"` // 用户名 给 swagger 看
	Captcha string `gorm:"comment:验证码" form:"captcha" json:"captcha" binding:"required"` // 用户名 给 swagger 看
	CaptchaId string `gorm:"comment:验证码id" form:"captcha_id" json:"captcha_id" binding:"required"` // 用户名 给 swagger 看

}