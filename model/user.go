package model

type User struct {
	Uuid string `json:"uuid"`
	Username string `json:"username"`
	Password string `json:"password"`
}