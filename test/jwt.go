package test

import (
	"gin-service/global"
	"gin-service/middleware"
	"gin-service/model/request"
)

// 生成tocken
func GenToken() string {
	// 获取密钥
	j := middleware.NewJWT()
	//fmt.Println(j)

	// 填充基本信息
	baseClaims := request.BaseClaims{
		Username: "小张",
	}

	// 生成完整struct
	completeStruct := j.CreateClaims(baseClaims)

	global.LOG.Info(completeStruct)

	//开始生成token
	token, err := j.CreateToken(completeStruct)
	if err != nil {
		panic(err)
	}

	return token
}
