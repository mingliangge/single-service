package api

import (
	"gin-service/global"
	"gin-service/model/request"
	"gin-service/model/response"
	"gin-service/service"
	"gin-service/utils"

	"github.com/gin-gonic/gin"
)

type User struct{}

// Register
// @tags 用户相关
// @summary 一段简明扼要的话
// @router /user/register [post]
// @param password formData string true "密码" minlength(1) maxlength(10)
// @param repeat_password formData string true "重复密码" minlength(1) maxlength(10)
// @param username formData string true "用户名"
func (*User) Register(c *gin.Context) {
	// 开始数据验证
	var r request.RegisterForm
	err := c.ShouldBind(&r)
	if err != nil {
		global.Verify(c, err)
		return
	}

	// 这里调用 server 层逻辑代码
	global.LOG.Info("看看")

	// 成功返回
	response.Ok(c)
}

func (*User) List(c *gin.Context) {
	response.Ok(c)
}

// Login
// @tags 用户相关
// @summary 用户登录
// @router /user/login [post]
// @param data body request.LoginForm true "大量数据"
func (*User) Login(c *gin.Context) {
	// 开始数据验证
	var r request.LoginForm
	err := c.ShouldBind(&r)
	if err != nil {
		global.Verify(c, err)
		return
	}

	// 验证码校验
	if !utils.CaptchaStore.Verify(r.CaptchaId, r.Captcha, true) {
		response.FailWithMessage("验证码错误", c)
		return
	}

	data := service.User.GenToken()

	response.OkWithMessage(data, c)
}

func (*User) GetData(c *gin.Context) {

	data := service.User.GetData()

	response.OkWithData(data, c)
}
