package global

import (
	"gin-service/config"
	"sync"
)

var CONFIG *config.ConfigData
var DEV bool
var wg sync.WaitGroup

// Init 工具类初始化
func Init() {
	wg.Add(4)
	go initGorm()
	go initRedis()
	go initZap()
	go initTranslator()
	wg.Wait()
}
