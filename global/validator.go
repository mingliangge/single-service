package global

import (
	"errors"
	"gin-service/model/response"
	"reflect"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"github.com/go-playground/locales/en"
	"github.com/go-playground/locales/zh"
	ut "github.com/go-playground/universal-translator"
	"github.com/go-playground/validator/v10"
	en_translations "github.com/go-playground/validator/v10/translations/en"
	zh_translations "github.com/go-playground/validator/v10/translations/zh"
)

var trans ut.Translator

// initTranslator 初始化翻译器
func initTranslator() {
	defer wg.Done()

	// 修改 gin 的 validator 属性，实现定制
	v, ok := binding.Validator.Engine().(*validator.Validate)
	if !ok {
		err := errors.New("修改validator属性失败")
		panic(err)
	}

	// 注册一个获取json的tag的自定义方法
	v.RegisterTagNameFunc(func(field reflect.StructField) string {
		name := strings.SplitN(field.Tag.Get("json"), ",", 2)[0]
		if name == "-" {
			return ""
		}
		return name
	})

	// 初始化中英文翻译器
	zhT := zh.New()
	enT := en.New()
	uni := ut.New(enT, zhT) // 后面英文是备用

	// 获取翻译器
	trans, ok := uni.GetTranslator("zh")

	// 注册翻译器
	var err error
	if ok {
		err = zh_translations.RegisterDefaultTranslations(v, trans)
	} else {
		err = en_translations.RegisterDefaultTranslations(v, trans)
	}

	if err != nil {
		panic(err)
	}
}

// removeTopStruct 截取 error 信息的 key
func removeTopStruct(fields map[string]string) map[string]string {

	rsp := map[string]string{}

	for field, err := range fields {
		// 获取 . 的位置
		subIndex := strings.Index(field, ".") + 1

		// 生成新的key
		newKey := field[subIndex:]

		// 将原来key修改
		rsp[newKey] = err
	}

	return rsp
}

// Verify 开始验证数据
func Verify(c *gin.Context, err error) {

	errs, ok := err.(validator.ValidationErrors)

	// 获取错误信息失败
	if !ok {
		response.FailWithMessage(err.Error(), c)
		return
	}

	// 翻译 + 截取信息
	errObj := removeTopStruct(errs.Translate(trans))

	// 返回退出
	response.FailWithDetailed(errObj, "验证失败", c)
}
