package global

import (
	"context"
	"fmt"

	"github.com/go-redis/redis/v8"
)

var RDB *redis.Client

var ctx = context.Background()

func initRedis() *redis.Client {
	defer wg.Done()

	rdb := redis.NewClient(&redis.Options{
		Addr:     CONFIG.Redis.ADDR,
		Password: CONFIG.Redis.PASSWORD,
		DB:       CONFIG.Redis.DB,
	})

	return rdb
}

// ExampleClient 示例
func ExampleClient() {
	rdb := redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "", // no password set
		DB:       0,  // use default DB
	})

	err := rdb.Set(ctx, "key", "value", 0).Err()
	if err != nil {
		panic(err)
	}

	val, err := rdb.Get(ctx, "key").Result()
	if err != nil {
		panic(err)
	}
	fmt.Println("key", val)

	val2, err := rdb.Get(ctx, "key2").Result()
	if err == redis.Nil {
		fmt.Println("key2 does not exist")
	} else if err != nil {
		panic(err)
	} else {
		fmt.Println("key2", val2)
	}
	// Output: key value
	// key2 does not exist
}
