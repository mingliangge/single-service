package router

import (
	"gin-service/api"
	"gin-service/middleware"
	"gin-service/model/response"
	"gin-service/test"
	"gin-service/utils"

	"github.com/gin-gonic/gin"
)

func InitUserRouter(r *gin.RouterGroup) {

	user := api.User{}

	g := r.Group("user")
	{
		g.POST("/register", user.Register)

		g.POST("/login", user.Login)
		//
		//g.GET("/list",middleware.JWTAuth(), api.List)
		//
		g.GET("/data", user.GetData)

		g.GET("/captcha", utils.GetCapcha)

		// 生成token
		g.GET("gen_token", genToken)

		g.GET("/token_use", middleware.JWTAuth(), tokenUse)
		//
		//g.POST("/ping", func(c *gin.Context) {
		//
		//	var loginForm request.RegisterForm
		//
		//	// 验证不通过就
		//	if err := c.ShouldBind(&loginForm); err != nil {
		//		fmt.Println(err.Error())
		//		c.JSON(http.StatusBadRequest,gin.H{
		//			"error" : err.Error(),
		//		})
		//		return
		//	}
		//
		//	c.JSON(200, gin.H{
		//		"message": "pong",
		//		"env":global.DEV,
		//	})
		//})
	}
}

// genToken 生成token
// Login
// @tags 用户相关
// @summary 生成token
// @router /user/gen_token [get]
// @security ApiKeyAuth
func genToken(context *gin.Context) {
	response.OkWithMessage(test.GenToken(), context)
}

// tokenUse 使用token
// Login
// @tags 用户相关
// @summary 使用token
// @router /user/token_use [get]
// @security ApiKeyAuth
func tokenUse(context *gin.Context) {
	response.Ok(context)
}
