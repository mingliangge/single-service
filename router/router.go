package router

import (
	"gin-service/global"
	"gin-service/model/response"

	"github.com/gin-gonic/gin"

	"gin-service/middleware"
)

var swagHandler gin.HandlerFunc

func Run() {

	var r *gin.Engine

	gin.SetMode(global.CONFIG.Gin.Mode)

	r = gin.New()

	// 按照配置的规则放行跨域请求
	r.Use(middleware.CorsByRules())

	// 不使用代理
	err := r.SetTrustedProxies(nil)
	if err != nil {
		panic(err)
	}

	group := r.Group("")

	// 总路由注册
	group.Use()
	{
		InitUserRouter(group)
	}

	// 健康检查接口
	r.GET("/health", response.Ok)

	// 通过 go build -tags doc 才会编译 swagger 文档
	if swagHandler != nil {
		r.GET("/swagger/*any", swagHandler)
	}

	r.Run(":" + global.CONFIG.Gin.Port)
}
