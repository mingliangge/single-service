//go:build doc
// +build doc

package router

import (
	_ "gin-service/docs"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	"sync"
)

var once sync.Once

func init() {
	once.Do(func() {
		swagHandler = ginSwagger.WrapHandler(swaggerFiles.Handler)
	})
}
